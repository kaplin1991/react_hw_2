import React from 'react'

const Row = ({ head, children }) => {
 
            return( 
                <> 
                    { head ? <thead>{children}</thead> : <tr>{children}</tr> }  
                </>
            ) 
}

Row.defaultProps = {
    head: false
}

export default Row;