import React from 'react' 

import './cell.css'

const Cell = ({ type, cells, background, color, children, currency }) => {

    return(
        <>
            <td 
                colspan={cells}
                style={{
                    backgroundColor : background,
                    color : color
                }}
                className={type}
            >{children} { currency ? currency : '' } </td>  
        </>
    )
}

Cell.defaultProps = {
    type: 'text',
    cells: 1,
    background: 'transparent',
    color: 'black'
  }
  
export default Cell;