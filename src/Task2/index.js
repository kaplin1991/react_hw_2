
/*
  Задание 2.
  1. Получить в методе component componentDidMount данные с
  https://jsonplaceholder.typicode.com/users

  2. Записать их в стейт по шаблону:
  {
    interviewed: false,
    user: { Элемент с jsonGenerator}
  }

  3. Вывести их в виде списка, каждый элемент которого будет являтся
  Functional компонентом принимающим в себе обьект с прошлого пункта.

  Внури компонента должна быть вызвана функция, которая меняет значение
  interviewed с true на flase.
  Используя functional Component с задантя 1. (Button)

  Если оно true - выделить пункт зеленым,
  Если false - не выделять или выделить красным

*/

import React, { Component } from 'react'

import User from './user'

class Task2 extends Component{

    constructor( props ){
        super( props );


        this.state = {
            interviewed: false,
            users: []
        }        
    }

    componentDidMount = () => {
        fetch('https://jsonplaceholder.typicode.com/users')
            .then( res => res.json())
            .then( res => { 
                this.setState({ 
                    users: res
                });
            })
            
    }
 

    render = () => {
        
        const { users, interviewed } = this.state;
   

        return (
            <ul> 
                { 
                users.map( ({ name }, index) =>  
                    <User 
                        key={index}
                        name={name}
                        interviewed = {interviewed}
                    /> 
                ) 
                }
            </ul>
        )
    }

}

export default Task2;

