import React from 'react'

import './user.css'
import Button from '../Task1/button'

const User = ({ name, interviewed}) => {

    const buttonClick = ( el ) => { 
        
        interviewed = !interviewed

        if( interviewed ){
            el.target.parentElement.style.backgroundColor = 'green'
        }else(
            el.target.parentElement.style.backgroundColor = 'white'
            )

    }

    return (
        <>
            <li className='user_item'>
                {name}
                <Button 
                    value = 'Проинтервьюирован'
                    fun = {buttonClick}
                />
            </li>
        </>
    )
}

export default User;