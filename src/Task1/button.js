import React from 'react'


const Button = ({ value, fun, style }) => (
    <button onClick={fun} style={style}>{value}</button>
)
 

export default Button;