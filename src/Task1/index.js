/*

  Задание 1.
  Написать functional компонент, который будет выводить нам кнопку
  c определенными стилями, которая будет вполнять действие которое в
  неё передали.

*/

import React from 'react'

import Button from './button'

const buttonClick = () => {
    console.log('Button is clicked!')
}

const Task1 = () => {



    return(
        <>
            <div>Task 1</div>
            <Button 
                value = 'Please click me!'
                fun = {buttonClick}
                style = {{backgroundColor: '#A3A3E1'}}
            />
        </>
    )
}


export default Task1;