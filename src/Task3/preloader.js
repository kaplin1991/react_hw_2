import React from 'react'


const Preloader = ({ src }) => {

    return (
        <img src={src} alt="src"/>
    )
}


export default Preloader;