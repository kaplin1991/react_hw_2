import React, { Component } from 'react'

import './image.css'
import Preloader from './preloader'
import PreImage from'./pre.gif'



class Image extends Component {

    constructor(props) {
        super(props);

        this.state = {
            loaded: false,
            error: '',
            image: this.props.src
        }
    }

    componentDidMount = () => {  
         
        this.setState({ 
            loaded:true 
        }) 
        
    }

    render = () => {
        const {loaded, image, error} = this.state; 
         
        if( !loaded ){
            console.log('Preloader')
            return <Preloader src={PreImage}/>
        }else{
            return (
                <div className='image_div'  >
                    { error === '' ? <img src={image} alt={image}/> : 'Error!'}
                </div>)
        } 
        
    }
}

export default Image;
